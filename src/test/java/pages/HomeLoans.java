package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static utils.commonutils.*;
import static utils.commonutils.clickAnElement;

public class HomeLoans {
    private WebDriver driver;
    @FindBy(how= How.LINK_TEXT, using="Request a call back")
    private WebElement requestACallBackLink;

    public HomeLoans(WebDriver driver) {
         this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void clickOnRequestACallBackLink() {
        waitForElementToBeClickable(requestACallBackLink,driver);
        requestACallBackLink.click();
    }
    public void selectHomeLoanTopic(){
        WebElement root = driver.findElement(By.id("contact-form-shadow-root"));
        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        WebElement shadowdom = (WebElement) js1.executeScript("return arguments[0].shadowRoot", root);
        waitForElementToBeClickable(shadowdom.findElement(By.tagName("div")),driver);
        WebElement divTag = shadowdom.findElement(By.tagName("div"));
        WebElement inputtag1 = divTag.findElement(By.cssSelector("div>div>div>div>div:nth-of-type(2)>section>div:nth-of-type(2)>fieldset>div>div>div>div>label>input"));
        clickAnElement(inputtag1,driver);
        waitForElementToBeClickable(divTag.findElement(By.cssSelector("div>div>div>div>div:nth-of-type(2)>section>div:nth-of-type(3)>button>div>span")),driver);
        WebElement spanTag = divTag.findElement(By.cssSelector("div>div>div>div>div:nth-of-type(2)>section>div:nth-of-type(3)>button>div>span"));
        clickAnElement(spanTag,driver);
    }
}
