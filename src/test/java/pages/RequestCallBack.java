package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

import static utils.commonutils.clickAnElement;
import static utils.commonutils.waitForElementToBeClickable;

public class RequestCallBack {
    private WebDriver driver;
    @FindBy(how= How.CSS, using="homeLoanRadio")
    private WebElement newHomeLoansRadioButton;

    @FindBy(how= How.XPATH, using="//*[@class='sc-VigVT fROwuq']//label[2]")
    private WebElement selectCustomerType;

    @FindBy(how= How.ID, using="field-page-Page1-aboutYou-firstName")
    private WebElement firstNameTextbox;

    @FindBy(how= How.ID, using="field-page-Page1-aboutYou-lastName")
    private WebElement lastNameTextbox;

    @FindBy(how= How.ID, using="field-page-Page1-aboutYou-phoneNumber")
    private WebElement phoneNumberTextbox;

    @FindBy(how= How.ID, using="field-page-Page1-aboutYou-email")
    private WebElement emailTextbox;

    public RequestCallBack(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void enterCallBackForm(String fName1,String lName1,String state1,String phoneNumber1,String email1 ){
        String currentHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            if (!actual.equalsIgnoreCase(currentHandle)) {
            //Switch to the opened tab
                driver.switchTo().window(actual);
                selectCustomerType.click();
                firstNameTextbox.sendKeys(fName1);
                lastNameTextbox.sendKeys(lName1);

                //driver.findElement(By.xpath("//*[@class='sc-VigVT fROwuq']//label[2]")).click();
                //driver.findElement(By.id("field-page-Page1-aboutYou-firstName")).sendKeys(fName);
               // driver.findElement(By.id("field-page-Page1-aboutYou-lastName")).sendKeys(lName);

                WebElement element1= driver.findElement(By.xpath("//div[@class='body']//div[@class='css-1hwfws3 react-select__value-container']//div"));element1.click();
                Actions action = new Actions(driver);  action.moveToElement(element1); action.moveByOffset(50,50).click().build().perform();
                phoneNumberTextbox.sendKeys(phoneNumber1);
                emailTextbox.sendKeys(email1);
                //driver.findElement(By.id("field-page-Page1-aboutYou-phoneNumber")).sendKeys(phoneNumber);

                //driver.findElement(By.id("field-page-Page1-aboutYou-email")).sendKeys(email);

                WebElement element2= driver.findElement(By.xpath("//button[@id='page-Page1-btnGroup-submitBtn']/span"));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element2);
                element2.click();
            }}
    }
}
