package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static utils.commonutils.scrollIntoTheElementAndClick;
import static utils.commonutils.waitForElementToBeClickable;

public class HomePage {
    private WebDriver driver;
    @FindBy(how= How.ID, using="logo")
    private WebElement nabLogo;

    @FindBy(how= How.XPATH, using="//div[@class='textWithIconRed']/ul/li[6]/a")
    private WebElement homeLoansLink;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnHomeLoanLink(WebDriver driver) {
        scrollIntoTheElementAndClick(homeLoansLink,driver);
    }

    public String verifyHomePageTitle(){
        return driver.getTitle();
    }
    public boolean validateNABLogo(){
        return nabLogo.isDisplayed();
    }
}
