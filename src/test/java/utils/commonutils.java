package utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;
import org.testng.TestException;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class commonutils {
    public static long IMPLICIT_WAIT=120;
    public static WebDriverWait wait;
    private static int timeout = 100;

    public static void switchWindow(WebDriver driver){
        Set<String> windows = driver.getWindowHandles();
        for (String handle: windows){
            driver.switchTo().window(handle);
        }
    }

    public static Select getSelectOptions(WebElement element) {
        return new Select(element);
    }

    public static void setOption(WebElement element,String value)
    {
        getSelectOptions(element).selectByVisibleText(value);
    }

    public static void waitForElementToBeClickable(WebElement element,WebDriver driver) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            throw new TestException("The following element is not clickable: " + element);
        }
    }
    public static void scrollIntoTheElementAndClick(WebElement element,WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        waitForElementToClick(element,driver);
        js.executeScript("arguments[0].scrollIntoView()", element);
        js.executeScript("arguments[0].click();", element);
    }
    public static void clickAnElement(WebElement element,WebDriver driver) {
        waitForElementToClick(element,driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }


    public static void waitForElementToClick(WebElement element, WebDriver driver){
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(2, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return element;
            }
        });
    }

}
