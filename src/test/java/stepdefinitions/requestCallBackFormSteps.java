package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.HomeLoans;
import pages.HomePage;
import pages.RequestCallBack;
public class requestCallBackFormSteps {
    public WebDriver driver=null;
    RequestCallBack requestCallBack;
    HomeLoans homeLoans;
    @Given("I am on call back form")
    public void i_am_on_call_back_form() {
         throw new io.cucumber.java.PendingException();
    }

    @When("I enter {string},  {string}, {string}, {string},  {string}")
    public void i_enter_first_name_last_name_state_phone_number_email(String fName,String lName, String state, String phoneNumber, String email) {
        HomePage homepage=new HomePage(driver);
        homepage.clickOnHomeLoanLink(driver);
        homeLoans=new HomeLoans(driver);
        homeLoans.clickOnRequestACallBackLink();
        homeLoans.selectHomeLoanTopic();
        requestCallBack= new RequestCallBack(driver);
        requestCallBack.enterCallBackForm(fName,lName,state,phoneNumber,email);
        throw new io.cucumber.java.PendingException();
    }

    @Then("I should see Request received successfully message")
    public void i_should_see_request_received_successfully_message() {
        throw new io.cucumber.java.PendingException();
    }

}
