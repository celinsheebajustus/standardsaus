package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.HomeLoans;
import pages.HomePage;

public class homeLoanFormSteps {
    public WebDriver driver=null;
    HomeLoans homeLoans;
    @Given("I am on the Home page")
    public void i_am_on_the_home_page(){
        HomePage homepage=new HomePage(driver);
        homepage.clickOnHomeLoanLink(driver);
        homeLoans=new HomeLoans(driver);

    }

    @When("I click on  Request a call back link")
    public void i_click_on_request_a_call_back_link() {
        homeLoans.clickOnRequestACallBackLink();
    }

    @Then("I should see the Home loan form")
    public void i_should_see_the_home_loan_form() {
        homeLoans.selectHomeLoanTopic();
    }
}
