Feature: Enter User details on Call back form

  Scenario Outline: Navigate to call back form
  As a user
  I can navigate to the call back form page, enter user details
  So that I can verify that call back form is submitted successfully

    Given I am on call back form
    When I enter "<firsName>", "<lastName>", "<state>", "<phoneNumber>", "<email>"
    Then I should see Request received successfully message
    Examples:
      | firsName | lastName | state | phoneNumber | email          |
      | Robb     | Tim      | NSW   | 0426774322  | robb@gmail.com |
      | Dan      | Simon    | QLD   | 0432223344  | DanSim@gmail.com |



















