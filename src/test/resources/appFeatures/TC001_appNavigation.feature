Feature: App Navigation Feature

  Scenario: App Navigation
  As a user
  I can navigate to the NAB Web app
  So that I can verify that landing page is loaded correctly

    Given I am on the NAB Web app page
    When I click on  Home Loan link
    Then I should see the Home Loan page