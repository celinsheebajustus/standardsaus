Feature: Home Loan form Navigation Feature

  Scenario: Select home loan option
  As a user
  I can navigate to the Request a call back page
  So that I can verify that Home loan form is loaded correctly

    Given I am on the Home page
    When I click on  Request a call back link
    Then I should see the Home loan form